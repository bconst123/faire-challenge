//
//  Maker.swift
//  faire-challenge
//
//  Created by Bruno Augusto Constantino on 3/21/19.
//  Copyright © 2019 Bruno Augusto Constantino. All rights reserved.
//

import Foundation

class Maker{
    let id:String
    let title:String
    let description:String
    let price:Double
    let image:URL
    
    init(id:String, title:String, description:String, price:Double, image:URL) {
        self.id = id
        self.title = title
        self.description = description
        self.price = price
        self.image = image
    }

}

