//
//  CategoryViewController.swift
//  faire-challenge
//
//  Created by Bruno Augusto Constantino on 3/22/19.
//  Copyright © 2019 Bruno Augusto Constantino. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class CategoryViewControler: UITableViewController {
    var categories:Array<String> = []
    var arrRes = [[String:AnyObject]]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Alamofire.request("https://www.faire.com/api/category/new").responseJSON { (responseData) -> Void in
            if((responseData.result.value) != nil) {
                let swiftyJsonVar = JSON(responseData.result.value!)
                if let resData = swiftyJsonVar[].arrayObject {
                    self.arrRes = resData as! [[String:AnyObject]]
                }
               
                for category in self.arrRes {
                    let element = category["name"] as! String
                    var haveSub = false
                
                    if let subCategories = category["sub_categories"] {
                        for subcategory in subCategories as! [AnyObject]{
                            
                            let nameSub = subcategory["name"]
                            var elementSub = nameSub as! String
                            //elementSub += " in "
                            //elementSub += element
                            haveSub = true
                            self.categories.append(elementSub)
                        }
                    }
                    if !haveSub {
                        self.categories.append(element)
                    }
                }
                self.tableView.reloadData()
            }
        }
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return categories.count
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let row = indexPath.row
        self.navigationController?.pushViewController(MakerViewController(category: categories[row]), animated: true)
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let row = indexPath.row
        let categories = self.categories[row]
        
        let cell = UITableViewCell(style: UITableViewCell.CellStyle.default, reuseIdentifier: nil)
        cell.textLabel!.text = categories
        
        return cell
    }
}

