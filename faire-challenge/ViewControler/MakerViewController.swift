//
//  ViewController.swift
//  faire-challenge
//
//  Created by Bruno Augusto Constantino on 3/21/19.
//  Copyright © 2019 Bruno Augusto Constantino. All rights reserved.
//

import UIKit
import SDWebImage
import Alamofire
import SwiftyJSON

class MakerViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout {
    
    var makers = Array<Maker>()
    var chosenCategory: String?
    var arrRes = [[String:AnyObject]]()
    
    init(category: String) {
        super.init(nibName: "MakerView", bundle: nil)
        self.chosenCategory = category
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.collectionView.register(UINib(nibName: "ViewCell", bundle:nil), forCellWithReuseIdentifier: "Cell")
        
        let urlString = "https://www.faire.com/api/search/makers-with-filters"
        
        if (chosenCategory == nil) { return }
        print(chosenCategory ?? "NULL")
        Alamofire.request(urlString, method: .post, parameters: ["SearchMakersWithFiltersRequest": chosenCategory!],encoding: JSONEncoding.default, headers: nil).responseJSON {
            response in
            switch response.result {
            case .success:
                print(response)
                //brand_products
                
                break
            case .failure(let error):
                
                print(error)
            }
        }
        
        
        
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 187, height: 255)
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let imageURL = URL(string: "https://images.pexels.com/photos/1131373/pexels-photo-1131373.jpeg?cs=srgb&dl=business-caffeine-coffee-1131373.jpg&fm=jpg") {
            makers.append(Maker(id: "b_2eyciz2x", title: "Maker Title", description: "Maker description", price: 1920.00, image: imageURL))
        }
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath)
        
        //cell.backgroundColor = .black
        //let cellData = self.products[indexPath.row]
        
        let imageView:UIImageView = cell.viewWithTag(200) as! UIImageView
        let labelTitle:UILabel = cell.viewWithTag(201) as! UILabel
        let labelPrice:UILabel = cell.viewWithTag(202) as! UILabel
        
        imageView.sd_setImage(with: makers[0].image)
        
        let description = makers[0].description
        let price = makers[0].price
        labelTitle.text = makers[0].title
        labelPrice.text = "\(description) -  $\(price)"
        
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print(indexPath.row)
        showProducts(index: indexPath.row)
    }
    
    func showProducts(index: Int) {
        self.navigationController?.pushViewController(ProductViewController(maker: makers[0]), animated: true)
    }

}

