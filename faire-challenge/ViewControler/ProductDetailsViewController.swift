//
//  ViewController.swift
//  faire-challenge
//
//  Created by Bruno Augusto Constantino on 3/23/19.
//  Copyright © 2019 Bruno Augusto Constantino. All rights reserved.
//

import UIKit
import SDWebImage
import Alamofire
import SwiftyJSON

class ProductDetailsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    var productToDetail: Product?
    var arrRes = [[String:AnyObject]]()
    @IBOutlet var titleField : UILabel!
    @IBOutlet var descriptionField : UILabel!
    @IBOutlet var imageView : UIImageView!
    @IBOutlet var tableView: UITableView!
    @IBOutlet var ratingBar : UIProgressView!
    @IBOutlet var ratingNumber : UILabel!
    var ratePercentage = 0
    var commentsArray = Array<String>()
    
    init(product: Product) {
        super.init(nibName: "ProductDetailsView", bundle: nil)
        self.productToDetail = product
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let product = productToDetail {
            imageView.sd_setImage(with: product.image)
            
            titleField.text = product.title
            var description = product.description
            description += " - $"
            description += String(format: "%.1f", product.price)
            
            descriptionField.text = description
        }
        
        /*Since I do not have the API I created mockAPIJSONforReview to be like my Reviews API Response*/
        let mockResponseAPIJSONforReview = "{\"rate_percentage\":88,\"comments\":[\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Nunc id cursus metus aliquam eleifend mi\",\"Vitae proin sagittis nisl rhoncus mattis. Sed libero enim sed faucibus turpis in eu mi. Vitae aliquet nec ullamcorper sit amet\",\"Et leo duis ut diam quam nulla porttitor massa id. Sagittis purus sit amet volutpat consequat mauris. In est ante in nibh mauris cursus mattis molestie.\"]}"
        
        var dictonary:NSDictionary?
        
        if let data = mockResponseAPIJSONforReview.data(using: String.Encoding.utf8) {
            
            do {
                dictonary = try JSONSerialization.jsonObject(with: data, options: []) as? [String:AnyObject] as NSDictionary?
                
                if let myDictionary = dictonary
                {
                    print(" Percentage: \(myDictionary["rate_percentage"]!)")
                    ratePercentage = myDictionary["rate_percentage"] as! Int
                    
                    if let comments = myDictionary["comments"] {
                        for comment in comments as! [AnyObject] {
                            print(comment)
                            commentsArray.append(comment as! String)
                        }
                    }
                    
                }
            } catch let error as NSError {
                print(error)
            }
        }
        self.tableView.rowHeight = UITableView.automaticDimension
        self.tableView.estimatedRowHeight = 44
        
        tableView.reloadData()
        
        ratingBar.progress = Float(ratePercentage)/Float(100)
        ratingNumber.text = String(ratePercentage)+"%"
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return commentsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let row = indexPath.row
        let eachComment = commentsArray[row]
        let cell = UITableViewCell(style: UITableViewCell.CellStyle.default, reuseIdentifier: nil)
        cell.textLabel?.text = eachComment
        cell.textLabel?.minimumScaleFactor = 0.1
        cell.textLabel?.font = UIFont.systemFont(ofSize: 10.0)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let row = indexPath.row
        print(row)
        let alert = UIAlertController(title: "Full Review", message: commentsArray[row], preferredStyle: UIAlertController.Style.alert)
        
        let ok = UIAlertAction(title: "got it", style: UIAlertAction.Style.cancel, handler: nil)
        
        alert.addAction(ok)
        
        present(alert, animated: false)
    }
    
    @IBAction func showReviewScreen() {
        self.navigationController?.pushViewController(RecommendationViewController(product: productToDetail!), animated: true)
    }
    
}

