//
//  ProductViewController.swift
//  faire-challenge
//
//  Created by Bruno Augusto Constantino on 3/22/19.
//  Copyright © 2019 Bruno Augusto Constantino. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class ProductViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout {

    var products = Array<Product>()
    var chosenMaker: Maker?
    
    init(maker: Maker) {
        super.init(nibName: "ProductViewController", bundle: nil)
        self.chosenMaker = maker
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print(chosenMaker!.title)
        
        self.collectionView.register(UINib(nibName: "ViewCell", bundle:nil), forCellWithReuseIdentifier: "Cell")
        
        if let id = chosenMaker?.id {
            let urlString = "https://www.faire.com/api/brand/\(id)/products"
        
            var arrRes = [[String:AnyObject]]()
            
            Alamofire.request(urlString).responseJSON { (responseData) -> Void in
                if((responseData.result.value) != nil) {
                    let swiftyJsonVar = JSON(responseData.result.value!)
                    if let resData = swiftyJsonVar[].arrayObject {
                        arrRes = resData as! [[String:AnyObject]]
                    }
                    
                    print(arrRes)
                }
            }
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 187, height: 255)
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let imageURL = URL(string: "https://upload.wikimedia.org/wikipedia/commons/1/15/Red_Apple.jpg") {
            products.append(Product(id: "v_12331221", title: "Product Title", description: "Product description", price: 990.00, image: imageURL))
        }
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath)
        
        //cell.backgroundColor = .black
        //let cellData = self.products[indexPath.row]
        
        let imageView:UIImageView = cell.viewWithTag(200) as! UIImageView
        let labelTitle:UILabel = cell.viewWithTag(201) as! UILabel
        let labelPrice:UILabel = cell.viewWithTag(202) as! UILabel

        imageView.sd_setImage(with: products[0].image)

        let description = products[0].description
        let price = products[0].price
        labelTitle.text = products[0].title
        labelPrice.text = "\(description) -  $\(price)"
        
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.navigationController?.pushViewController(ProductDetailsViewController(product: products[0]), animated: true)
    }
    
}
