//
//  RecommendationViewController.swift
//  faire-challenge
//
//  Created by Bruno Augusto Constantino on 3/23/19.
//  Copyright © 2019 Bruno Augusto Constantino. All rights reserved.
//

import UIKit

class RecommendationViewController: UIViewController {
    var productToRecommend: Product?
    
    init(product: Product) {
        super.init(nibName: "RecommendationView", bundle: nil)
        self.productToRecommend = product
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    @IBOutlet var imageView : UIImageView!
    @IBOutlet var titleLabel : UILabel!
    @IBOutlet var slider : UISlider!
    @IBOutlet var reviewField : UITextField!
    
    override func viewDidLoad() {
        imageView.sd_setImage(with: productToRecommend!.image)
        titleLabel.text = productToRecommend!.title
    }
    
    @IBAction func sendReview() {
        print("Review send!")
        /*should send review to the review API*/
    }
}
